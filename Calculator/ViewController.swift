//
//  ViewController.swift
//  Calculator
//
//  Created by Tine Ramos on 3/5/15.
//  Copyright (c) 2015 Lancera. All rights reserved.
//  Acknowledgement to Stanford University
//

import UIKit
import Foundation

class ViewController: UIViewController
{
    @IBOutlet weak var display: UILabel!
    @IBOutlet weak var history: UILabel!
    
    var userIsInTheMiddleOfTypingANumber = false
    var allowMultipleZero = false
    var historyValue = String()
    
    var brain = CalculatorBrain()
    
    @IBAction func appendDigit(sender: UIButton)
    {
        let digit = sender.currentTitle!
        let hasDecimal = (display.text!).rangeOfString(".")
        
        if digit.toInt() > 0 || hasDecimal != nil {
            allowMultipleZero = true
        }
        
        if userIsInTheMiddleOfTypingANumber {
            if hasDecimal != nil && digit == "." {
                return
            }
            else {
                if digit == "0" && !allowMultipleZero {
                    return
                }
                display.text = display.text! + digit
            }
        }
        else {
            display.text = digit
            userIsInTheMiddleOfTypingANumber = true
        }
    }
    
    @IBAction func enter()
    {
        userIsInTheMiddleOfTypingANumber = false
        appendHistory(display.text!)
        if let result = brain.pushOperand(displayValue) {
            displayValue = result
        }
        else {
            displayValue = 0
        }
    }
    
    //TODO: make this optional
    var displayValue: Double {
        get {
            return NSNumberFormatter().numberFromString(display.text!)!.doubleValue
        }
        
        set {
            display.text = "\(newValue)"
            userIsInTheMiddleOfTypingANumber = false
        }
    }
    
    func appendHistory(value: String) {
        historyValue += "\(value) "
        history.text = historyValue
    }
    
    @IBAction func operate(sender: UIButton) {
        if userIsInTheMiddleOfTypingANumber {
            enter()
        }
        
        if let operation = sender.currentTitle {
            appendHistory(operation)
            if let result = brain.performOperation(operation) {
                displayValue = result
            }
            else {
                displayValue = 0
            }
        }
        
//        NOTE: replaced by code above
//        let operation = sender.currentTitle!
//        appendHistory(operation)
//        switch operation {
//            case "+": performOperation({$0 + $1})
//            case "−": performOperation({$1 - $0})
//            case "×": performOperation({$0 * $1})
//            case "÷": performOperation({$1 / $0})
//            case "√": performOperation({sqrt($0)})
//            case "sin": performOperation({sin($0)})
//            case "cos": performOperation({cos($0)})
//            case "π": performOperation(M_PI)
//            default: break
//        }
    }
    
    @IBAction func clear()
    {
        display.text = "0"; history.text = "0"
        historyValue = String()
        brain.clearStack()
        userIsInTheMiddleOfTypingANumber = false
        allowMultipleZero = false
    }
    
    /* removed because of methods in CalculatorBrain
    func performOperation(operation: (Double, Double) -> Double)
    {
        if operandStack.count >= 2 {
            displayValue = operation(operandStack.removeLast(), operandStack.removeLast())
            enter()
        }
    }
    
    func performOperation(operation: Double -> Double)
    {
        if operandStack.count >= 1 {
            displayValue = operation(operandStack.removeLast())
            enter()
        }
    }
    
    func performOperation(value: Double)
    {
        if (display.text != "0") {
            displayValue = value
        }
        enter()
    }
    */
    
    @IBAction func backspace() {
        if display.text != "0" && userIsInTheMiddleOfTypingANumber {
            
            if countElements(display.text!) > 1 {
                display.text = dropLast(display.text!)
            }
            else {
                display.text = "0"
                userIsInTheMiddleOfTypingANumber = false
            }
            
            historyValue = countElements(historyValue) > 1 ? dropLast(historyValue) : "0"
            history.text = historyValue
        }
    }
    
}
